import numpy as np
import matplotlib.pyplot as plt

a=0.; 
b=2.;
t0=a;
y0=1.;
def f(t,y):
    return y;
def yd(t):
    return np.exp(t);

method='midpoint';
er=[];
for n in [20,40,80,160,320]:
    t=np.linspace(a,b,n+1);
    h=(b-a)/n;
    
    ydd=[];
    for k in range(n+1):
        ydd.append(yd(t[k]))
    
    if method=='explicit':
        ya=[];
        ya.append(y0)
        for k in range(n):
            yar=ya[k]+h*f(t[k],ya[k]);
            ya.append(yar)
    
    elif method=='implicit':
         ya=[];
         ya.append(y0)
         for k in range(n):
            yar=1/(1-h)*ya[k];
            ya.append(yar)
            
    elif method=='trapez': 
        ya=[];
        ya.append(y0);
        for k in range(n):
            yar=(1+0.5*h)/(1-0.5*h)*ya[k];
            ya.append(yar);
        
    elif method=='midpoint': 
        ya=[];
        ya.append(y0);
        ya.append(y0+h*f(t0,y0))
        for k in range(1,n):
            yar=ya[k-1]+2*h*ya[k];
            ya.append(yar);
    
    plt.figure();
    plt.plot(t,ya,'.',t,ydd,'.')
    
    e=np.max(np.abs(np.array(ya)-np.array(ydd)))
    er.append(e)

print('2^a = ', np.array(er[0:len(er)-1])/np.array(er[1:len(er)]))
plt.show()