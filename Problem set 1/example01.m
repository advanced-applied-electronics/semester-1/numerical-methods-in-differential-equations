%3x_{n+1}-10x_{n}+3x_{n-1}=0
%x_{0}=1, x_{1}=1/3, x_{2}=1/9
%x_{n}=(1/3)^n

clear all;
x=[1,1/3];
xr1=1; xr2=1/3;
for n=1:60
  xr=(10*xr2-3*xr1)/3;
  xr1=xr2;
  xr2=xr;
  x=[x,xr2];
endfor
xn=[x(62),x(61)];
xr1=xn(1);xr2=xn(2);
for n=60:-1:1
  xr=(10*xr2-3*xr1)/3;
  xr1=xr2;
  xr2=xr;
  xn=[xn,xr2];
  endfor
  

