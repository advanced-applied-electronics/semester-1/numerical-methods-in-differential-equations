import numpy as np
from numpy import pi
from scipy.sparse import spdiags
from scipy.sparse import *
import scipy
import matplotlib.pyplot as plt
from scipy.linalg import lu

# ----------------------------------


def p():
    return 1


def q(x):
    return x+1


def f(x):
    return (pi**2 + x + 1)*np.sin(pi*x)


def y(x):
    return np.sin(pi*x)


def ydx(x):
    return pi*np.sin(pi*x)


def H1error(coord, h, uh, u, udx):
    nvert = np.max(coord.size)
    x = np.array([])
    k = 0
    for i in range(nvert-1):
        xm = (coord[i+1]+coord[i])/2
        x = np.block([x, coord[i], xm])
        k = k+2

    ndof = k+1
    x[ndof-2] = coord[nvert-1]
    uq = u(x)
    uxq = udx(x)
    L2err = 0
    H1err = 0
    for i in range(nvert-1):
        one = (uh[i]-uq[2*i-1])
        two = (0.5 * uh[i]+0.5*uh[i+1]-uq[2*i])
        L2err = L2err + (h/6)*(one**2 + 4*two**2+(uh[i+1]-uq[2*i+1])**2)
        H1err = H1err + (1/(6*h))*((uh[i+1]-uh[i]-h*uxq[2*i-1])**2+4 * (
            uh[i+1]-uh[i]-h*uxq[2*i])**2+(uh[i+1]-uh[i]-h*uxq[2*i+1])**2)
    H1err = np.sqrt(H1err + L2err)
    L2err = np.sqrt(L2err)

    return L2err, H1err

# ----------------------------------


a = 0
b = 1
y0 = 0
y1 = 0
k = -1
error = np.zeros(5)
error2 = np.array([])
her = np.zeros(5)
# for index, n in enumerate([50, 100, 200, 400, 800]):
for index, n in enumerate([50, 100]):
    k = k+1
    x = np.linspace(a, b, n+1)
    h = (b-a)/n

    M = np.zeros((n-1, n-1))

    v0 = np.zeros(n-1)
    v1 = np.zeros(n-2)
    v2 = np.zeros(n-2)
    for i in range(n-1):
        xr = x[i+1]
        v0[i] = 1/h*(p()+p()) + h/3*(q(xr+h/2) + q(xr-h/2))

    v0 = np.transpose(v0)

    for i in range(n-2):
        xr = x[i+2]
        v2[i] = -1/h*p()+h/6*q(xr-h/2)
        xr = x[i+1]
        v1[i] = -1/h*p()+h/6*q(xr-h/2)

    v0 = v0.reshape((n-1, 1))
    v1 = np.transpose(np.block([v1, 0])).reshape((n-1, 1))
    v2 = np.transpose(np.block([0, v2])).reshape((n-1, 1))
    D = np.array([[-1], [0], [1]])

    block = np.block([[v1, v0, v2]]).T
    M = spdiags(block, D, n-1, n-1)

    fb = np.zeros(n-1)
    for i in range(n-1):
        xr = x[i+1]
        fb[i] = h/2*(f(xr+h/2) + f(xr-h/2))

    matrix = M.A

    P, L, U = lu(matrix)

    fb = np.dot(P, np.transpose(fb))
    z = np.dot(np.linalg.inv(L), fb)
    yp = np.dot(np.linalg.inv(U), z).reshape((n-1, 1))

    ypp = np.block([[y0], [yp], [y1]])

    yp = np.reshape(yp, (1, n-1))
    yd = y(x[1:-1]).reshape((1, n-1))

    error[k] = np.max(np.abs(yp-yd))
    her[k] = h
    L2err, H1err = H1error(x, h, ypp, y, ydx)

    error2 = np.block([[error2], [L2err, H1err]])

    argx = []
    argy = []
    argy_exact = []
    for X in range(n-1):
        argx.append(x[X+1])
    for aa in yp.reshape(n-1):
        argy.append(aa)
    for ar in argx:
        argy_exact.append(y(ar))

    plot1 = plt.figure(index)
    plt.plot(argx, argy, '.', color="green", label='estimated')
    plt.plot(argx, argy_exact, '.', color="red", label='exact')
    plt.legend()

plt.show()
