""" Numerical methods in differential equations.

    Laboratory 3 
    
    Prepared by: Piotr Zieliński
    
    Problem 1.B:
    Use the shooting algorithm to approximate the solution of the
    following boundary value problem with accuracy = 0:001 at the right end
    of the interval. The actual solution is given for comparison to your result.
"""
from cmath import log
import numpy as np
from numpy import pi, polyfit
from scipy.sparse import *
import matplotlib.pyplot as plt
from scipy.linalg import lu

import numpy as np
import matplotlib.pyplot as plt
import math
import time

def y_exact(x):
    return (1/(x+1))

def fg(yr, tr)
    return [[yr[1]], ]

a = 1
y_a = 0.5 # y(a)
b = 2
y_b = 2 # y(b)

n = 100
t = np.linspace(a, b, n)
h = (b-a)/n;