import numpy as np
from numpy import pi
from scipy.sparse import spdiags
from scipy.sparse import *
import scipy
import matplotlib.pyplot as plt
from scipy.linalg import lu
import math

# ----------------------------------


def p():
    return 3


def q(x):
    return -2


def f(x):
    return 2*x+3


def y(x):
    return -3 + 5*np.exp((-3/2 + np.sqrt(17)/2)*x) - x


# def ydx(x):
#     return pi*np.sin(pi*x)


a = 0
b = 1
y0 = 2
y1 = -4 + 5*np.exp(-3/2 + np.sqrt(17)/2)
k = -1
error = np.zeros(5)
herror = np.zeros(5)
her = np.zeros(5)
for index, n in enumerate([50, 100, 200, 400, 1800]):
    # for index, n in enumerate([50, 100]):
    k = k+1
    t = np.linspace(a, b, n+1)
    h = (b-a)/n

    M = np.zeros((n-1, n-1))

    for i in range(n-1):
        M[i, i] = 2+q(t[i+1])*(h**2)

    for i in range(n-2):
        M[i, i+1] = -1+p()*h/2
        M[i+1, i] = -1-p()*h/2

    fb = np.zeros(n-1)
    for i in range(n-1):
        fb[i] = f(t[i+1])

    fb = fb*(h**2)
    fb[0] = fb[0] - y0*(-1-p()*h/2)
    fb[-1] = fb[-1] - y1*(-1-p()*h/2)

    yp = np.zeros(n-1)
    yp = np.dot(np.linalg.inv(M), np.transpose(fb)).reshape((n-1, 1))

    yp = np.block([[y0], [yp], [y1]])
    # print(yp)

    yd = y(t)
    error[k] = np.max(np.abs(yp-yd))
    herror[k] = h

    argx = []
    argy = []
    argy_exact = []
    for x_value in t:
        argx.append(x_value)
    for y_value in yp:
        argy.append(y_value)
    for ar in argx:
        argy_exact.append(y(ar))

    plot1 = plt.figure(index)
    plt.plot(argx, argy, '.', color="green", label='estimated')
    plt.plot(argx, argy_exact, '.', color="red", label='exact')
    plt.legend()

plt.show()
