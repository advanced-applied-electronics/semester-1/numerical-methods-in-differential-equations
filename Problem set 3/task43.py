""" Numerical methods in differential equations.

    Laboratory 3 
    
    Prepared by: Piotr Zieliński
    
    Problem 4.3:
        Use the Ritz-Galerkin method to approximate the solution to
        each of the following boundary value problems. The actual solution yd(x) is
        given for comparison purposes.
"""
from cmath import log
import numpy as np
from numpy import pi, polyfit
from scipy.sparse import *
import matplotlib.pyplot as plt
from scipy.linalg import lu

# ----------------------------------


def p():
    return 1


def q(x):
    return x+1


def f(x):
    return (pi**2 + x + 1)*np.sin(pi*x)


def y(x):
    return np.sin(pi*x)


def ydx(x):
    return pi*np.cos(pi*x)


def H1error(coord, h, uh, u, udx):
    """ This function computes the error in the H1-norm.
    """
    nvert = np.max(coord.size)
    x = np.array([])
    k = 0
    for i in range(nvert-1):
        xm = (coord[i+1]+coord[i])/2
        x = np.block([x, coord[i], xm])
        k = k+2

    ndof = k+1
    x[ndof-2] = coord[nvert-1]
    uq = u(x)
    uxq = udx(x)
    L2err = 0
    H1err = 0
    for i in range(nvert-1):
        one = (uh[i]-uq[2*i-1])
        two = (0.5 * uh[i]+0.5*uh[i+1]-uq[2*i])
        L2err = L2err + (h/6)*(one**2 + 4*two**2+(uh[i+1]-uq[2*i+1])**2)
        H1err = H1err + (1/(6*h))*((uh[i+1]-uh[i]-h*uxq[2*i-1])**2+4 * (
            uh[i+1]-uh[i]-h*uxq[2*i])**2+(uh[i+1]-uh[i]-h*uxq[2*i+1])**2)
    H1err = np.sqrt(H1err + L2err)
    L2err = np.sqrt(L2err)

    return L2err, H1err

# ----------------------------------


a = 0
b = 1
y0 = 0
y1 = 0
k = -1
error = np.zeros(5)
error2 = np.zeros([5, 2])
her = np.zeros(5)

for index, n in enumerate([50, 100, 200, 400, 800]):
    k = k+1
    x = np.linspace(a, b, n+1)
    h = (b-a)/n

    # Construction of the main matrix
    M = np.zeros((n-1, n-1))

    v0 = np.zeros(n-1)
    v1 = np.zeros(n-2)
    v2 = np.zeros(n-2)
    for i in range(n-1):
        xr = x[i+1]
        v0[i] = 1/h*(p()+p()) + h/3*(q(xr+h/2) + q(xr-h/2))

    v0 = np.transpose(v0)

    for i in range(n-2):
        xr = x[i+2]
        v2[i] = -1/h*p()+h/6*q(xr-h/2)
        xr = x[i+1]
        v1[i] = -1/h*p()+h/6*q(xr-h/2)

    v0 = v0.reshape((n-1, 1))
    v1 = np.transpose(np.block([v1, 0])).reshape((n-1, 1))
    v2 = np.transpose(np.block([0, v2])).reshape((n-1, 1))
    D = np.array([[-1], [0], [1]])

    block = np.block([[v1, v0, v2]]).T
    M = spdiags(block, D, n-1, n-1)

    # Construction of the vector on the right-hand side
    fb = np.zeros(n-1)
    for i in range(n-1):
        xr = x[i+1]
        fb[i] = h/2*(f(xr+h/2) + f(xr-h/2))

    matrix = M.A
    
    # Approximate solution
    P, L, U = lu(matrix)

    fb = np.dot(P, np.transpose(fb))
    z = np.dot(np.linalg.inv(L), fb)
    yp = np.dot(np.linalg.inv(U), z).reshape((n-1, 1))

    ypp = np.block([[y0], [yp], [y1]])
    
    # Vector of exact values
    yd = y(x).reshape((n+1, 1))

    # Global error
    error[k] = np.max(np.abs(ypp-yd))
    her[k] = h
    L2err, H1err = H1error(x, h, ypp, y, ydx)
    
    error2[k] = [L2err, H1err]

    # Pointwise errors
    argx = []
    argy = []
    argy_exact = []
    arg_error = []
    for x_value in x:
        argx.append(x_value)
    for y_value in ypp:
        argy.append(y_value)
    for ar in argx:
        argy_exact.append(y(ar))
    for er in np.abs(ypp-yd):
        arg_error.append(er)

    plot1 = plt.figure(index)
    plt.plot(argx, argy, '.', color="purple", label='Estimated solution')
    plt.plot(argx, argy_exact, '.', color="green", label='Exact solution')
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("y(t)")
    plotName = "Ritz-Galerkin method for n=" + str(n)
    plt.title(plotName)
    plt.grid()
    plt.savefig("Results_4_3/"+plotName+".png")

    plot1 = plt.figure(index+6)
    plt.plot(argx, arg_error, '.', color="brown", label='Error')
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("Error")
    plotName = "Error of solution estimation for n=" + str(n)
    plt.title(plotName)
    plt.grid()
    plt.savefig("Results_4_3/"+plotName+".png")

# Prepare order of the error plot
loger =-np.log(error2[:,0].T)
logher = -np.log(her)
pr = polyfit(logher, loger, 1)

plot1 = plt.figure(index+7)
plt.plot(logher, loger, '.', color="red", linestyle='-', linewidth=1)
plt.plot(logher, pr[0]*logher+pr[1], '.', color="red", linestyle='-', linewidth=1)
plt.legend()
plotName = "Order of the error"
plt.title(plotName)
plt.grid()
plt.savefig("Results_4_3/"+plotName+".png")

plt.show()
