# Numerical methods in differential equations

**Table of contents**

[[_TOC_]]

## Link to lecture / lab page

[Link](http://mydlarczyk.kft.pwr.edu.pl/r2122l/numerical_2122l/metnum_2122l.php)

## Problem set 1

## Problem set 2 (Homework 1)

Your task is to solve two exercises. The main parts of your solutions should consist of Matlab codes with attached clarifying them block diagrams and your conclusions. Deadline is 25.03. Solutions will be graded in the scale 0 - 5 pts.

## Problem set 3 (Homework 2)

Your task is to solve two exercises. The main part of your solutions should consist of Matlab codes with attached clarifying them block diagrams and your conclusions. Your Matlab or Python codes should be sent in a seperate text file (or files ). Deadline is 13.05. Solutions will be graded in the scale 0 - 5 pts.

## Problem set 4 & 5 (Homework 3)

Your task is to solve an exercise from the problem set 4. Additionaly those of you who want to improve your grade account may solve an exercise from the problem set 5. The main part of your solutions should consist of Matlab codes with attached clarifying them block diagrams and your conclusions. Your Matlab or Python codes should be sent in a seperate text file (or files ). Deadline is 17.06. Solutions will be graded in the scale 0 - 5 pts.

## Literature

- [1] J. C. Butcher, Numerical Methods for Ordinary Differential Equations, John Wiley & Sons 2003

- [2] A. Quarteroni, R. Sacco, F. Saleri, Numerical Mathematics, Springer Berlin Heidelberg 2007

- [3] K. W. Morton, D. F. Mayers, Numerical Solution of Partial Differential Equations. An Introduction, Cambridge University Press 2005

- [4] Richard L. Burden, J. Douglas Faires, Numerical Analysis

- [5] R. M. Mattheij, S. W. Rienstra, J.H.M. ten Thije Boonkkamp, Partial Differential Equations. Modeling, Analysis, Computation

- [6] Z. Fortuna, B. Macukow, J. Wąsowicz, Metody Numeryczne, WNT Warszawa 2003
