""" Python script for "Numerical Methods in Differential Equations" classes.
    Problem set: 2
    Task: 4D.
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import itertools
from solver import iterative_approx_solver as solver

# Prepare directory for saving figures
def path(file_name, dbgprint=True):
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    figure_dir = os.path.join(script_dir, "fig_T4D")
    
    # Create figure_dir if don't exist
    if not os.path.exists(figure_dir):
        os.mkdir(figure_dir)
    
    file_path = os.path.join(figure_dir, file_name)
    if dbgprint:
        print(F"Plot saved to \"{file_path}\".\n")
    return file_path

def legend_without_duplicate_labels(ax):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique), loc='best')

# Formula for calculation of next y approximation in implicit method:
def y_n_plus_1_implicit_Euler(y_n, t_n, h, func_t_y=None):
    y_n_plus_1 = (y_n + 20*h*np.sin(t_n+h) + h*np.cos(t_n+h))
    y_n_plus_1 /= (1 + 20*h)
    return y_n_plus_1

# Formula for calculation of next y approximation in trapezoidal method:
def y_n_plus_1_trapezoidal(y_n, t_n, h, func_t_y=None):
    y_n_plus_1 = y_n + (h/2)*((-20)*y_n + 20*np.sin(t_n) +
                            np.cos(t_n) + 20*np.sin(t_n+h) + np.cos(t_n+h))
    y_n_plus_1 /= (1 + 10*h)
    return y_n_plus_1

# dy/dt is function of t and y
def func_t_y(t, y):
    return -20*y + 20 * np.sin(t) + np.cos(t)

# Formula for exact solution
def exact_solution(t):
    return np.exp(-20*t) + np.sin(t)


# -----------| Task 4D solution |---------------------
show_plots = False

print('-'*100)
print('Python script for "Numerical Methods in Differential Equations" classes\nProblem set: 2\nTask: 4D\n')

# Intervals
t_start = 0.
t_interval1 = 0.2
t_interval2 = 1.0

# Steps sizes for first and second interval
step_interval1 = 0.01
step_interval2 = 0.05
step_size_divs = []
for i in range(5):
    step_size_divs.append(2**i)

# Initial value
y_initial = 1

# Solve this task using given methods:
methods = ["explicitEuler",
           "implicitEuler",
           "trapezoidal",
           #"midpoint",
           "rungeKutta2",
           "rungeKutta4"]
    
# Prepare list for comparsion of methods
cmp_y_approx = [[[] for step_size_div in step_size_divs] for method in methods]
cmp_approx_err = [[[] for step_size_div in step_size_divs] for method in methods]
cmp_t = [[[] for step_size_div in step_size_divs] for method in methods]
cmp_global_errors = [[[] for step_size_div in step_size_divs] for method in methods]
compare_number_of_steps = [[[] for step_size_div in step_size_divs] for method in methods]
compare_step_size = [[[] for step_size_div in step_size_divs] for method in methods]

for (m_id, method), (s_id, step_size_div) in itertools.product(enumerate(methods), enumerate(step_size_divs)):
    # Solve initial value problem for interval 0-0.2 using iterative approximate solver
    [t,
     y_approx,
     y_exact,
     y_approx_err,
     step_size,
     number_of_steps]=solver(func_t_y=func_t_y,
                             t_initial=t_start,
                             t_final=t_interval1,
                             y_initial=y_initial,
                             method=method,
                             step_size=step_interval1/step_size_div,
                             exact_solution=exact_solution,
                             y_n_plus_1_implicit=y_n_plus_1_implicit_Euler,
                             y_n_plus_1_trapezoidal=y_n_plus_1_trapezoidal,
                             debug_print=False)
     
    # Append results from first interval
    cmp_global_errors[m_id][s_id].append(np.max(y_approx_err))
    compare_number_of_steps[m_id][s_id].append(number_of_steps)
    compare_step_size[m_id][s_id].append(step_size)

    # Solve initial value problem for interval 0.2-1 using iterative approximate solver
    [t_1,
     y_approx_1,
     y_exact_1,
     y_approx_err_1,
     step_size1,
     number_of_steps1]=solver(func_t_y=func_t_y,
                              t_initial=t_interval1,
                              t_final=t_interval2,
                              y_initial=y_approx[-1],
                              method=method,
                              step_size=step_interval2/step_size_div,
                              exact_solution=exact_solution,
                              y_n_plus_1_implicit=y_n_plus_1_implicit_Euler,
                              y_n_plus_1_trapezoidal=y_n_plus_1_trapezoidal,
                              debug_print=False)

    # Append results from second interval
    cmp_global_errors[m_id][s_id].append(np.max(y_approx_err_1))
    compare_number_of_steps[m_id][s_id].append(number_of_steps1)
    compare_step_size[m_id][s_id].append(step_size1)

    # Merge results
    t = np.append(t[:-1], t_1)
    y_approx = np.append(y_approx[:-1], y_approx_1)
    y_exact = np.append(y_exact[:-1], y_exact_1)
    y_approx_err = np.append(y_approx_err[:-1], y_approx_err_1)
    
    # Append results to comparsion lists
    cmp_y_approx[m_id][s_id].append(y_approx)
    cmp_approx_err[m_id][s_id].append(y_approx_err)
    cmp_t[m_id][s_id].append(t)
    
 
    # Create subplots
    fig, (ax_result, ax_errors) = plt.subplots(nrows=2, sharex='row')
       
    # Plotting of y_approx and y_exact
    ax_result.set_title(F"Results of approximation for {method} method. Step size h/{step_size_div}.")
    ax_result.set_xlabel("t")
    ax_result.set_ylabel("y(t)")
    ax_result.grid(True)
    ax_result.plot(t, y_approx, '.', color="green", label="approximated")
    ax_result.plot(t, y_exact, '-', color="red", label="exact")
    ax_result.legend()
    
    # Ploting of approx error
    ax_errors.set_title(F"Error of approximation for {method} method. Step size h/{step_size_div}.")
    ax_errors.set_xlabel("t")
    ax_errors.set_ylabel("Approximation error")
    ax_errors.grid(True)
    ax_errors.plot(t, y_approx_err, '.', color="purple", label="approximation error")
    ax_errors.legend()
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'4D'}_results_{method}_h_div_{step_size_div}.png"), bbox_inches='tight')    
    if not show_plots:
        plt.close(fig)
        
# Compare results of one method for changing step size
colors = ["blue", "purple", "yellow", "green", "orange", "pink"]
for (m_id, method) in enumerate(methods):
    # Ploting of result comparsion plot
    print(F"Ploting results comparsion plot of method {method} for variable step size.\n")
    fig, ax = plt.subplots()
    ax.set_title(F"Results of approximation for {method} method. Step size variable")
    ax.set_xlabel("t")
    ax.set_ylabel("y(t)")
    ax.plot(t, y_exact, '-', color="red", label="exact")
    ax.grid(True)

    for (s_id, step_size_div), color in zip(enumerate(step_size_divs), colors):
        ax.plot(cmp_t[m_id][s_id], cmp_y_approx[m_id][s_id], '.', color=color, label=F"step size h/{step_size_div}", markersize=5)

    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4D'}_results_comparsion_for_{method}_method.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
    
    # Ploting of error comparsion plot
    print(F"Ploting error comparsion plot of method {method} for variable step size.\n")
    fig, ax = plt.subplots()
    ax.set_title(F"Errors of approximation for {method} method. Step size variable")
    ax.set_xlabel("t")
    ax.set_ylabel("Approximation error")
    ax.grid(True)

    for (s_id, step_size_div), color in zip(enumerate(step_size_divs), colors):        
        ax.plot(cmp_t[m_id][s_id], cmp_approx_err[m_id][s_id], '.', color=color, label=F"step size h/{step_size_div}", markersize=5)

    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4D'}_error_comparsion_for_{method}_method.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

# Compare results of methods for given step size
for (s_id, step_size_div) in enumerate(step_size_divs):
    # Ploting of result comparsion plot
    print(F"Ploting methods result comparsion for step size h/{step_size_div}")
    fig, ax = plt.subplots()
    ax.set_title(F"Results of approximation for step size h/{step_size_div}")
    ax.set_xlabel("t")
    ax.set_ylabel("y(t)")
    ax.plot(t, y_exact, '-', color="red", label="exact")
    ax.grid(True)
    colors = ["blue", "purple", "yellow", "green", "orange", "pink"]

    for (m_id, method), color in zip(enumerate(methods), colors):        
        ax.plot(cmp_t[m_id][s_id], cmp_y_approx[m_id][s_id], '.', color=color, label=method, markersize=5)
    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4D'}_results_comparsion_for_h_div_{step_size_div}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

    # Ploting of error comparsion plot
    print(F"Ploting methods error comparsion for step size h/{step_size_div}")
    fig, ax = plt.subplots()
    ax.set_title(F"Error of approximation for step size h/{step_size_div}")
    ax.set_xlabel("t")
    ax.set_ylabel("Approximation error")
    ax.grid(True)
    colors = ["blue", "purple", "yellow", "green", "orange", "pink"]

    for (m_id, method), color in zip(enumerate(methods), colors):
        ax.plot(cmp_t[m_id][s_id], cmp_approx_err[m_id][s_id], '.', color=color, label=method, markersize=5)  
    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4D'}_error_comparsion_for_h_div_{step_size_div}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

# Prepare convergence plot by sweeping over methods with variable stepsize
for m_id, method in enumerate(methods):
    # Prepare matrixes for ploting
    steps = np.log(np.array(compare_step_size[m_id]).copy())
    steps = np.dot(steps, -1)
    global_errors = np.log(np.array(cmp_global_errors[m_id]).copy())
    global_errors = np.dot(global_errors, -1)
    
    fig, (ax_first, ax_second) = plt.subplots(ncols=2)
  
    # Linear regresion for first interval
    b, a = np.polynomial.polynomial.polyfit(steps[:, 0], global_errors[:, 0], deg=1)
    reg_line = np.dot(a, steps[:, 0]) + b
    
    # Plot for first interval
    ax_first.set_title(F"Convergence plot for {method},\n first interval\nreglin:{a:.2f}*x + {b:.2f}")
    #ax_first.set_xscale('log')
    #ax_first.set_yscale('log')
    ax_first.set_xlabel("ln(h)")
    ax_first.set_ylabel("ln(e(h))")
    ax_first.plot(steps[:, 0], global_errors[:, 0], 'o', color='blue', markersize=5, label="ln of glob error")
    ax_first.plot(steps[:, 0], reg_line, '--', color='red', label="reglin")
    ax_first.grid(visible=True, which='both', axis='both')
    legend_without_duplicate_labels(ax_first)

    # Linear regresion for second interval
    b, a = np.polynomial.polynomial.polyfit(steps[:, 1], global_errors[:, 1], deg=1)
    reg_line = np.dot(a, steps[:, 1]) + b
    
    # Second interval plot
    ax_second.set_title(F"Convergence plot for {method},\n second interval\nreglin:{a:.2f}*x + {b:.2f}")
    #ax_second.set_xscale('log')
    #ax_second.set_yscale('log')
    ax_second.set_xlabel("ln(h)")
    ax_second.set_ylabel("ln(e(h))")
    ax_second.plot(steps[:, 1], global_errors[:, 1], 'o', color='green', markersize=5, label="ln of glob error")
    ax_second.plot(steps[:, 1], reg_line, '--', color='red', label="reglin")
    ax_second.grid(visible=True, which='both', axis='both')
    legend_without_duplicate_labels(ax_second)
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'4D'}_convergence_for_method_{method}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

# Analyse order of method for first interval
print("\nAnalyse order of method for first interval")
for m_id, method in enumerate(methods):
    errors = np.array(cmp_global_errors[m_id])
    ratios = np.log2(errors[0:-1, 0]/errors[1:, 0])
    print(F"Order of {method} method = {ratios}")

# Analyse order of method for second interval
print("\nAnalyse order of method for second interval")
for m_id, method in enumerate(methods):
    errors = np.array(cmp_global_errors[m_id])
    ratios = np.log2(errors[0:-1, 1]/errors[1:, 1])
    print(F"Order of {method} method = {ratios}")
    
print(F'Please check output plots in directory: "{path("", False)}"!!!!!!!')
        
# Show all plots
if show_plots:
    plt.show()