import numpy as np

# Formula for calculation of next y approximation in implicit method must be defined by user with such arguments:
# def y_n_plus_1_implicit(y_n, t_n, h, func_t_y):

# Formula for calculation of next y approximation in trapezoidal method must be defined by user with such arguments:
# def y_n_plus_1_trapezoidal(y_n, t_n, h, func_t_y):

method_names = ["explicitEuler",
                "implicitEuler",
                "trapezoidal",
                "midpoint",
                "rungeKutta2",
                "rungeKutta4"]

def iterative_approx_solver(
        t_initial,
        t_final,
        method,
        y_initial,
        step_size=None,
        func_t_y=None,
        y_n_plus_1_implicit=None,
        y_n_plus_1_trapezoidal=None,
        number_of_steps=None,
        exact_solution=None,
        debug_print=False):
    
    if debug_print:
        print("\n"+"------"*15)

    if (step_size is None and number_of_steps is None):
        raise AttributeError("Number of steps or step size must be given.")

    if (step_size is not None and number_of_steps is not None):
        raise AttributeError("Cant use both number of steps and step size.")

    if (method not in method_names):
        raise AttributeError(
            F"Wrong method name given. Possible methods are: {method_names}")

    if (method == "implicitEuler" and y_n_plus_1_implicit is None):
        raise AttributeError(
            "Formula for implicit euler method calculation was not given.")

    if (method == "trapezoidal" and y_n_plus_1_trapezoidal is None):
        raise AttributeError(
            "Formula for trapezoidal method calculation was not given.")

    # Prepare number of steps if it wasn't given
    if number_of_steps is None:
        number_of_steps = (t_final - t_initial)
        number_of_steps /= step_size
        number_of_steps = int(np.round(number_of_steps))

    # Prepare step size if it wasn't given
    if step_size is None:
        step_size = (t_final - t_initial) / number_of_steps
    
    # DBG print
    if debug_print:
        print(F"Approximation using {method} method on interval {t_initial} < t < {t_final}, with initial value y({t_initial})={y_initial}.")
        print(F"Step size {step_size} ({number_of_steps} steps).")
        
    # Prepare t mesh of points
    t = np.linspace(t_initial, t_final, number_of_steps)

    # Define array for approximated values
    y_approx = np.empty(number_of_steps)

    # Iterate using chosen method
    match method:
        case "explicitEuler":
            # Initial y value
            y_approx[0] = y_initial

            # Iterate through all t interval
            for i in range(1, number_of_steps):
                approx = y_approx[i-1] + step_size * func_t_y(t[i-1], y_approx[i-1])
                y_approx[i] = approx
        case "implicitEuler":
            # Initial y value
            y_approx[0] = y_initial

            # Iterate through all t interval
            for i in range(1, number_of_steps):
                approx = y_n_plus_1_implicit(y_n=y_approx[i-1],
                                             t_n=t[i-1],
                                             h=step_size,
                                             func_t_y=func_t_y)
                y_approx[i] = approx

        case "midpoint":
            # Initial y value
            y_approx[0] = y_initial

            # 2nd value of y approximation
            approx = y_initial + step_size * func_t_y(t_initial, y_initial)
            y_approx[1] = approx

            # Iterate through the rest of the t interval
            for i in range(2, number_of_steps-1):
                approx = y_approx[i-2] + 2 * step_size * func_t_y(t[i-1], y_approx[i-1])
                y_approx[i] = approx

        case "trapezoidal":
            # Initial y value
            y_approx[0] = y_initial

            # Iterate through the rest of the t interval
            for i in range(1, number_of_steps):
                approx = y_n_plus_1_trapezoidal(y_n=y_approx[i-1],
                                                t_n=t[i-1],
                                                h=step_size,
                                                func_t_y=func_t_y)
                y_approx[i] = approx
                
        case "rungeKutta2":
            # Initial y value
            y_approx[0] = y_initial

            # Iterate through the rest of the t interval
            for i in range(1, number_of_steps):
                k1 = func_t_y(t[i-1], y_approx[i-1]) * step_size
                k2 = func_t_y(t[i-1] + step_size/2, y_approx[i-1] + k1/2) * step_size
                approx = y_approx[i-1] + k2
                y_approx[i] = approx

        case "rungeKutta4":
            # Initial y value
            y_approx[0] = y_initial

            # Iterate through the rest of the t interval, calculatin approx for i+1
            for i in range(1, number_of_steps):
                k1 = func_t_y((t[i-1]), (y_approx[i-1])) * step_size
                k2 = func_t_y((t[i-1] + step_size/2), (y_approx[i-1] + k1/2)) * step_size
                k3 = func_t_y((t[i-1] + step_size/2), (y_approx[i-1] + k2/2)) * step_size
                k4 = func_t_y((t[i-1] + step_size  ), (y_approx[i-1] + k3  )) * step_size
                approx = y_approx[i-1] + k1/6 + k2/3 + k3/3 + k4/6    # for i+1
                y_approx[i] = approx

        case _:
            raise AssertionError("Unhandled method")
    
    # Exact solution & errors
    y_exact = np.array([])
    y_approx_err = np.array([])
    if (exact_solution is not None):
        # Calculate exact values
        y_exact = exact_solution(t)
        # Calculate approx errors
        y_approx_err = np.abs(y_exact - y_approx)
            
    return [t, y_approx.copy(), y_exact.copy(), y_approx_err.copy(), step_size, number_of_steps]

def iterative_approx_solver_2D(
        t_initial,
        t_final,
        method,
        y_initial,
        step_size=None,
        func_t_y=None,
        y_n_plus_1_implicit=None,
        y_n_plus_1_trapezoidal=None,
        number_of_steps=None,
        exact_solution=None,
        debug_print=False):
    
    if debug_print:
        print("\n"+"------"*15)

    if (step_size is None and number_of_steps is None):
        raise AttributeError("Number of steps or step size must be given.")

    if (step_size is not None and number_of_steps is not None):
        raise AttributeError("Cant use both number of steps and step size.")

    if (method not in method_names):
        raise AttributeError(
            F"Wrong method name given. Possible methods are: {method_names}")

    if (method == "implicitEuler" and y_n_plus_1_implicit is None):
        raise AttributeError(
            "Formula for implicit euler method calculation was not given.")

    if (method == "trapezoidal" and y_n_plus_1_trapezoidal is None):
        raise AttributeError(
            "Formula for trapezoidal method calculation was not given.")

    # Prepare number of steps if it wasn't given
    if number_of_steps is None:
        number_of_steps = (t_final - t_initial)
        number_of_steps /= step_size
        number_of_steps = int(np.round(number_of_steps))

    # Prepare step size if it wasn't given
    if step_size is None:
        step_size = (t_final - t_initial) / number_of_steps
    
    # DBG print
    if debug_print:
        print(F"Approximation using {method} method on interval {t_initial} < t < {t_final}, with initial value y({t_initial})={y_initial}.")
        print(F"Step size {step_size} ({number_of_steps} steps).")
        
    # Prepare t mesh of points
    t = np.linspace(t_initial, t_final, number_of_steps)

    # Define array for approximated values
    y_approx = np.empty((number_of_steps,2))

    # Iterate using chosen method
    match method:
        case "explicitEuler":
            # Initial y value
            y_approx[0, :] = y_initial

            # Iterate through all t interval
            for i in range(1, number_of_steps):
                approx = y_approx[i-1] + step_size * func_t_y(t[i-1], y_approx[i-1])
                y_approx[i] = approx
        case "implicitEuler":
            # Initial y value
            y_approx[0, :] = y_initial

            # Iterate through all t interval
            for i in range(1, number_of_steps):
                approx = y_n_plus_1_implicit(y_n=y_approx[i-1],
                                             t_n=t[i-1],
                                             h=step_size,
                                             func_t_y=func_t_y)
                y_approx[i] = approx

        case "midpoint":
            # Initial y value
            y_approx[0, :] = y_initial

            # 2nd value of y approximation
            approx = y_initial + step_size * func_t_y(t_initial, y_initial)
            y_approx[1] = approx

            # Iterate through the rest of the t interval
            for i in range(2, number_of_steps-1):
                approx = y_approx[i-2] + 2 * step_size * func_t_y(t[i-1], y_approx[i-1])
                y_approx[i] = approx

        case "trapezoidal":
            # Initial y value
            y_approx[0, :] = y_initial

            # Iterate through the rest of the t interval
            for i in range(1, number_of_steps):
                approx = y_n_plus_1_trapezoidal(y_n=y_approx[i-1],
                                                t_n=t[i-1],
                                                h=step_size,
                                                func_t_y=func_t_y)
                y_approx[i] = approx
                
        case "rungeKutta2":
            # Initial y value
            y_approx[0, :] = y_initial

            # Iterate through the rest of the t interval
            for i in range(1, number_of_steps):
                k1 = func_t_y(t[i-1], y_approx[i-1]) * step_size
                k2 = func_t_y(t[i-1] + step_size/2, y_approx[i-1] + k1/2) * step_size
                approx = y_approx[i-1] + k2
                y_approx[i] = approx

        case "rungeKutta4":
            # Initial y value
            y_approx[0, :] = y_initial

            # Iterate through the rest of the t interval, calculatin approx for i+1
            for i in range(1, number_of_steps):
                k1 = func_t_y((t[i-1]), (y_approx[i-1])) * step_size
                k2 = func_t_y((t[i-1] + step_size/2), (y_approx[i-1] + k1/2)) * step_size
                k3 = func_t_y((t[i-1] + step_size/2), (y_approx[i-1] + k2/2)) * step_size
                k4 = func_t_y((t[i-1] + step_size  ), (y_approx[i-1] + k3  )) * step_size
                approx = y_approx[i-1] + k1/6 + k2/3 + k3/3 + k4/6    # for i+1
                y_approx[i] = approx

        case _:
            raise AssertionError("Unhandled method")
    
    # Exact solution & errors
    y_exact = np.array([])
    y_approx_err = np.array([])
    if (exact_solution is not None):
        # Calculate exact values
        y_exact = exact_solution(t)
        # Calculate approx errors
        y_approx_err = np.abs(y_exact - y_approx)
            
    return [t, y_approx.copy(), y_exact.copy(), y_approx_err.copy(), step_size, number_of_steps]