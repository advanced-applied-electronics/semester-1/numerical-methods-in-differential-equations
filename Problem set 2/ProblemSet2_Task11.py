""" Python script for "Numerical Methods in Differential Equations" classes.
    Problem set: 2
    Task: 11.
"""
from matplotlib import rcParams
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import itertools
from solver import iterative_approx_solver_2D as solver2D

# Prepare directory for saving figures
def path(file_name, dbgprint=True):
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    figure_dir = os.path.join(script_dir, "fig_T11")
    
    # Create figure_dir if don't exist
    if not os.path.exists(figure_dir):
        os.mkdir(figure_dir)
    
    file_path = os.path.join(figure_dir, file_name)
    if dbgprint:
        print(F"Plot saved to \"{file_path}\".\n")
    return file_path


def func_t_y(t_n, y_n):
    func = np.array([y_n[1], # y
                     -y_n[1] +np.sign(y_n[1]) - y_n[0]]) # -y + sign(y) - x
    return func


# -----------| Task 4D solution |---------------------
rcParams['figure.figsize'] = 10, 8
print('-'*100)
print('Python script for "Numerical Methods in Differential Equations" classes\nProblem set: 2\nTask: 11\n')
show_plots = True
t_start=0
t_final = [10]
number_of_steps = 10000

#-----| Initial values: |---------
# First solution
y_1_initial=np.array([0, 0.5])
# second solution
y_2_initial=np.array([0, 2.2])

methods = [#"explicitEuler",
           #"implicitEuler",
           #"trapezoidal",
           #"midpoint",
           #"rungeKutta2",
           "rungeKutta4"]

for (m_id, method), (t_id, t_final)  in itertools.product(enumerate(methods), enumerate(t_final)):
    # Get first solution (for starting point 1)
    [t_1,
     y_approx_1,
     _,        
     _,
     step_size_1,
     number_of_steps_1]=solver2D(func_t_y=func_t_y,
                             t_initial=t_start,
                             t_final=t_final,
                             y_initial=y_1_initial,
                             method=method,
                             number_of_steps=number_of_steps,
                             debug_print=True)
     
    # Get second solution (for starting point 2)
    [t_2,
     y_approx_2,
     _,
     _,
     step_size_2,
     number_of_steps_2]=solver2D(func_t_y=func_t_y,
                             t_initial=t_start,
                             t_final=t_final,
                             y_initial=y_2_initial,
                             method=method,
                             number_of_steps=number_of_steps,
                             debug_print=True)


    # Create subplots for separate solutions
    fig, (ax_sol1, ax_sol2) = plt.subplots(ncols=2, sharex=True)
    
    # Create segment line
    line = np.column_stack((y_1_initial, y_2_initial))
       
    # Plotting of first solution
    ax_sol1.set_title(F"First solution for {method} method\n Final t = {t_final}")
    ax_sol1.set_xlabel("x(t)")
    ax_sol1.set_ylabel("y(t)")
    ax_sol1.grid(True)
    ax_sol1.plot(y_approx_1[:,0], y_approx_1[:,1], '--', color="green", label=F"solution 1", markersize=1)
    ax_sol1.plot(line[0], line[1], '-', color="red", label="segment line", markersize=1)
    ax_sol1.annotate(F"S1: {y_1_initial}", xy=y_1_initial, xytext=y_1_initial+[-1,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='top')
    ax_sol1.annotate(F"S2: {y_2_initial}", xy=y_2_initial, xytext=y_2_initial+[-1,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    ax_sol1.plot(line[0], line[1], 'o', color="red", markersize=5)
    # Plot endpoint E1
    approx_endpoint1 = np.array([0.0, 1.257])
    ax_sol1.plot(*approx_endpoint1, 'o', color="green", markersize=5)
    ax_sol1.annotate(F"E1: {approx_endpoint1}", xy=approx_endpoint1, xytext=approx_endpoint1+[-1,-0.4],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    ax_sol1.legend()
    
    # Ploting of second solution
    ax_sol2.set_title(F"Second solution for {method} method\n Final t = {t_final}")
    ax_sol2.set_xlabel("x(t)")
    ax_sol2.set_ylabel("y(t)")
    ax_sol2.grid(True)
    ax_sol2.plot(y_approx_2[:,0], y_approx_2[:,1], '--', color="orange", label=F"solution 2", markersize=1)
    ax_sol2.plot(line[0], line[1], '-', color="red", label="segment line", markersize=1)
    ax_sol2.annotate(F"S1: {y_1_initial}", xy=y_1_initial, xytext=y_1_initial+[-1,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='top')
    ax_sol2.annotate(F"S2: {y_2_initial}", xy=y_2_initial, xytext=y_2_initial+[-1,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    ax_sol2.plot(line[0], line[1], 'o', color="red", markersize=5)
    # Plot endpoint E2
    approx_endpoint2 = np.array([0.0, 1.309])
    ax_sol2.plot(*approx_endpoint2, 'o', color="orange", markersize=5)
    ax_sol2.annotate(F"E2: {approx_endpoint2}", xy=approx_endpoint2, xytext=approx_endpoint2+[-1,+0.5],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    ax_sol2.legend()
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'11'}_results_{method}.png"), bbox_inches='tight')    
    if not show_plots:
        plt.close(fig)
        
    # Create subplots with boht solutions
    fig, ax = plt.subplots()
    
    # Create segment line
    line = np.column_stack((y_1_initial, y_2_initial))
       
    # Plotting of first solution
    ax.set_title(F"Proof for periodic solution existance\nApprox method: {method}\n Final t = {t_final}")
    ax.set_xlabel("x(t)")
    ax.set_ylabel("y(t)")
    ax.grid(True)
    ax.plot(y_approx_1[:,0], y_approx_1[:,1], '--', color="green", label=F"solution 1", markersize=1)
    ax.plot(y_approx_2[:,0], y_approx_2[:,1], '--', color="orange", label=F"solution 2", markersize=1)
    # Plot segment line
    ax.plot(line[0], line[1], '-', color="red", label="segment line", markersize=1)
    # Plot starting points
    ax.plot(line[0], line[1], 'o', color="red", markersize=5)
    ax.annotate(F"S1: {y_1_initial}", xy=y_1_initial, xytext=y_1_initial+[-0.7,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='top')
    ax.annotate(F"S2: {y_2_initial}", xy=y_2_initial, xytext=y_2_initial+[-0.7,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    # Plot endpoint E1
    approx_endpoint1 = np.array([0.0, 1.257])
    ax.plot(*approx_endpoint1, 'o', color="green", markersize=5)
    ax.annotate(F"E1: {approx_endpoint1}", xy=approx_endpoint1, xytext=approx_endpoint1+[-0.7,-0.4],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    # Plot endpoint E2
    approx_endpoint2 = np.array([0.0, 1.309])
    ax.plot(*approx_endpoint2, 'o', color="orange", markersize=5)
    ax.annotate(F"E2: {approx_endpoint2}", xy=approx_endpoint2, xytext=approx_endpoint2+[-1,+0.5],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    ax.legend()
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'11'}_periodic_sol_ex_proof_{method}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
        
    # Attempt to find and plot periodic solution
    y_per_initial = np.array([0, 1.27])
    
    [t_per,
     y_approx_per,
     _,
     _,
     step_size_per,
     number_of_steps_per]=solver2D(func_t_y=func_t_y,
                             t_initial=t_start,
                             t_final=t_final,
                             y_initial=y_per_initial,
                             method=method,
                             number_of_steps=number_of_steps,
                             debug_print=True)
     
    # Create subplot with periodic solution
    fig, ax = plt.subplots()
    
    # Create segment line
    line = np.column_stack((y_1_initial, y_2_initial))
       
    # Plotting of first solution
    ax.set_title(F"Attempt to find periodic solution\nApprox method: {method}\n Final t = {t_final}")
    ax.set_xlabel("x(t)")
    ax.set_ylabel("y(t)")
    ax.grid(True)
    ax.plot(y_approx_1[:,0], y_approx_1[:,1], '--', color="green", label=F"solution 1", markersize=1)
    ax.plot(y_approx_2[:,0], y_approx_2[:,1], '--', color="orange", label=F"solution 2", markersize=1)
    # Plot segment line
    ax.plot(line[0], line[1], '-', color="red", label="segment line", markersize=1)
    # Plot starting points
    ax.plot(line[0], line[1], 'o', color="red", markersize=5)
    ax.annotate(F"S1: {y_1_initial}", xy=y_1_initial, xytext=y_1_initial+[-0.7,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='top')
    ax.annotate(F"S2: {y_2_initial}", xy=y_2_initial, xytext=y_2_initial+[-0.7,-0.2],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    # Plot endpoint E1
    approx_endpoint1 = np.array([0.0, 1.257])
    ax.plot(*approx_endpoint1, 'o', color="green", markersize=5)
    ax.annotate(F"E1: {approx_endpoint1}", xy=approx_endpoint1, xytext=approx_endpoint1+[-0.7,-0.4],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    # Plot endpoint E2
    approx_endpoint2 = np.array([0.0, 1.309])
    ax.plot(*approx_endpoint2, 'o', color="orange", markersize=5)
    ax.annotate(F"E2: {approx_endpoint2}", xy=approx_endpoint2, xytext=approx_endpoint2+[-1,+0.5],
                     arrowprops={'arrowstyle': '->'},
                     horizontalalignment='left', verticalalignment='bottom')
    # Plot periodic solution
    ax.plot(y_approx_per[:,0], y_approx_per[:,1], '-', color="blue", label=F"periodic solution", linewidth=1)
    ax.legend()
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'11'}_periodic_sol_{method}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
    
print(F'Please check output plots in directory: "{path("", False)}"!!!!!!!')
        
# Show all plots
if show_plots:
    plt.show()


