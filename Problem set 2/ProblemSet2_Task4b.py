""" Python script for "Numerical Methods in Differential Equations" classes.
    Problem set: 2
    Task: 4B.
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import itertools
from solver import iterative_approx_solver as solver

# Prepare directory for saving figures
def path(file_name):
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    figure_dir = os.path.join(script_dir, "fig")
    
    # Create figure_dir if don't exist
    if not os.path.exists(figure_dir):
        os.mkdir(figure_dir)
    
    file_path = os.path.join(figure_dir, file_name)
    print(F"Plot saved to \"{file_path}\".\n")
    return file_path

def legend_without_duplicate_labels(ax):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique))

# Formula for calculation of next y approximation in implicit method:
def y_n_plus_1_implicit_Euler(y_n, t_n, h, func_t_y=None):
    y_n_plus_1 = (y_n + 20*h*np.sin(t_n+h) + h*np.cos(t_n+h))
    y_n_plus_1 /= (1 + 20*h)
    return float(y_n_plus_1)

# Formula for calculation of next y approximation in trapezoidal method:
def y_n_plus_1_trapezoidal(y_n, t_n, h, func_t_y=None):
    y_n_plus_1 = y_n + h/2*(-20*y_n + 20*np.sin(t_n) +
                            np.cos(t_n) + 20*np.sin(t_n+h) + np.cos(t_n+h))
    y_n_plus_1 /= (1 + 10*h)
    return float(y_n_plus_1)

# dy/dt is function of t and y
def func_t_y(t, y):
    return -7*(y-t)+1

# Formula for exact solution
def exact_solution(t):
    return t + 3 * np.e**(-7*t)


# -----------| Task 4B solution |---------------------
show_plots = False

# Intervals
t_start = 0.
t_interval = 1


# Initial value
y_initial = 3

# Solve this task using given methods:
methods = ["explicitEuler",
           "implicitEuler",
           "trapezoidal",
           "midpoint",
           "rungeKutta2",
           "rungeKutta4"]

# Steps sizes for first and second interval
step_size_divs = []
for i in range(5):
    step_size_divs.append(2**i)
    
# Prepare list for comparsion of methods
compare_y_approx = [[[] for step_size_div in step_size_divs] for method in methods]
compare_y_approx_err = [[[] for step_size_div in step_size_divs] for method in methods]
compare_t = [[[] for step_size_div in step_size_divs] for method in methods]
compare_y_global_error = [[[] for step_size_div in step_size_divs] for method in methods]
compare_number_of_steps = [[[] for step_size_div in step_size_divs] for method in methods]
compare_step_size = [[[] for step_size_div in step_size_divs] for method in methods]

for (m_id, method), (s_id, step_size_div) in itertools.product(enumerate(methods), enumerate(step_size_divs)):
    # Solve initial value problem for interval 0-0.2 using iterative approximate solver
    [t,
     y_approx,
     y_exact,
     y_approx_err,
     step_size,
     number_of_steps]=solver(func_t_y=func_t_y,
                             t_initial=t_start,
                             t_final=t_interval,
                             y_initial=y_initial,
                             method=method,
                             step_size=0.1/step_size_div,
                             exact_solution=exact_solution,
                             y_n_plus_1_implicit=y_n_plus_1_implicit_Euler,
                             y_n_plus_1_trapezoidal=y_n_plus_1_trapezoidal,
                             debug_print=True)
     
    # Append results from first interval
    compare_y_global_error[m_id][s_id].append(np.max(y_approx_err))
    compare_number_of_steps[m_id][s_id].append(number_of_steps)
    compare_step_size[m_id][s_id].append(step_size)

    # Append results to comparsion lists
    compare_y_approx[m_id][s_id].append(y_approx)
    compare_y_approx_err[m_id][s_id].append(y_approx_err)
    compare_t[m_id][s_id].append(t)
    
 
    # Create subplots
    fig, (ax_result, ax_errors) = plt.subplots(nrows=2, sharex='row')
       
    # Plotting of y_approx and y_exact
    ax_result.set_title(F"Results of approximation for {method} method. Step size h/{step_size_div}.")
    ax_result.set_xlabel("t")
    ax_result.set_ylabel("y(t)")
    ax_result.grid(True)
    ax_result.plot(t, y_approx, '.', color="green", label="approximated")
    ax_result.plot(t, y_exact, '-', color="red", label="exact")
    ax_result.legend(loc="upper left")
    
    # Ploting of approx error
    ax_errors.set_title(F"Error of approximation for {method} method. Step size h/{step_size_div}.")
    ax_errors.set_xlabel("t")
    ax_errors.set_ylabel("Approximation error")
    ax_errors.grid(True)
    ax_errors.plot(t, y_approx_err, '.', color="purple", label="approximation error")
    ax_errors.legend(loc="upper left")
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'4B'}_results_{method}_h_div_{step_size_div}.png"), bbox_inches='tight')    
    if not show_plots:
        plt.close(fig)
        
# Compare results of one method for changing step size
colors = ["blue", "purple", "yellow", "green", "orange", "pink"]
for (m_id, method) in enumerate(methods):
    # Ploting of result comparsion plot
    print(F"Ploting results comparsion plot of method {method} for variable step size.\n")
    fig, ax = plt.subplots()
    ax.set_title(F"Results of approximation for {method} method. Step size variable")
    ax.set_xlabel("t")
    ax.set_ylabel("y(t)")
    ax.plot(t, y_exact, '-', color="red", label="exact")
    ax.grid(True)

    for (s_id, step_size_div), color in zip(enumerate(step_size_divs), colors):
        ax.plot(compare_t[m_id][s_id], compare_y_approx[m_id][s_id], '.', color=color, label=F"step size h/{step_size_div}", markersize=5)

    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4B'}_results_comparsion_for_{method}_method.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
    
    # Ploting of error comparsion plot
    print(F"Ploting error comparsion plot of method {method} for variable step size.\n")
    fig, ax = plt.subplots()
    ax.set_title(F"Errors of approximation for {method} method. Step size variable")
    ax.set_xlabel("t")
    ax.set_ylabel("Approximation error")
    ax.grid(True)

    for (s_id, step_size_div), color in zip(enumerate(step_size_divs), colors):        
        ax.plot(compare_t[m_id][s_id], compare_y_approx_err[m_id][s_id], '.', color=color, label=F"step size h/{step_size_div}", markersize=5)

    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4B'}_error_comparsion_for_{method}_method.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

# Compare results of methods for given step size
for (s_id, step_size_div) in enumerate(step_size_divs):
    # Ploting of result comparsion plot
    print(F"Ploting methods result comparsion for step size h/{step_size_div}")
    fig, ax = plt.subplots()
    ax.set_title(F"Results of approximation for step size h/{step_size_div}")
    ax.set_xlabel("t")
    ax.set_ylabel("y(t)")
    ax.plot(t, y_exact, '-', color="red", label="exact")
    ax.grid(True)
    colors = ["blue", "purple", "yellow", "green", "orange", "pink"]

    for (m_id, method), color in zip(enumerate(methods), colors):        
        ax.plot(compare_t[m_id][s_id], compare_y_approx[m_id][s_id], '.', color=color, label=method, markersize=5)    
    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4B'}_results_comparsion_for_h_div_{step_size_div}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

    # Ploting of error comparsion plot
    print(F"Ploting methods error comparsion for step size h/{step_size_div}")
    fig, ax = plt.subplots()
    ax.set_title(F"Error of approximation for step size h/{step_size_div}")
    ax.set_xlabel("t")
    ax.set_ylabel("Approximation error")
    ax.grid(True)
    colors = ["blue", "purple", "yellow", "green", "orange", "pink"]

    for (m_id, method), color in zip(enumerate(methods), colors):
        ax.plot(compare_t[m_id][s_id], compare_y_approx_err[m_id][s_id], '.', color=color, label=method, markersize=5)  
    legend_without_duplicate_labels(ax)
    fig.savefig(path(F"PS{'2'}_T{'4B'}_error_comparsion_for_h_div_{step_size_div}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

# Prepare convergence plot by sweeping over methods with variable stepsize
for m_id, method in enumerate(methods):
    # Prepare matrixes for ploting
    steps = np.log(np.array(compare_step_size[m_id]).copy())
    steps = np.dot(steps, -1)
    global_errors = np.log(np.array(compare_y_global_error[m_id]).copy())
    global_errors = np.dot(global_errors, -1)
    
    fig, (ax_first, ax_second) = plt.subplots(ncols=2)
    
    # First interval
    ax_first.set_title(F"Convergence plot for {method},\n first interval")
    ax_first.set_xscale('log')
    ax_first.set_yscale('log')
    ax_first.set_xlabel("ln(h)")
    ax_first.set_ylabel("ln(e(h))")
    ax_first.grid(True)
    ax_first.plot(steps[:, 0], global_errors[:, 0], 'o--', color='blue', markersize=5)
    
    # Second interval plot
    ax_second.set_title(F"Convergence plot for {method},\n second interval")
    ax_second.set_xscale('log')
    ax_second.set_yscale('log')
    ax_second.set_xlabel("ln(h)")
    ax_second.set_ylabel("ln(e(h))")
    ax_second.grid(True)
    ax_second.plot(steps[:, 0], global_errors[:, 0], 'o--', color='green', markersize=5)
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'2'}_T{'4B'}_convergence_for_method_{method}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)

# Analyse order of method for first interval
print("\nAnalyse order of method for first interval")
for m_id, method in enumerate(methods):
    errors = np.array(compare_y_global_error[m_id]).copy()
    ratios = np.log2(np.divide(errors[0:-1, 0], errors[1:, 0]))
    print(F"Order of {method} method = {ratios} = {np.round(np.average(ratios))}")

# Analyse order of method for second interval
print("\nAnalyse order of method for second interval")
for m_id, method in enumerate(methods):
    errors = np.array(compare_y_global_error[m_id]).copy()
    ratios = np.log2(np.divide(errors[0:-1, 0], errors[1:, 0]))
    print(F"Order of {method} method = {ratios} = {np.round(np.average(ratios))}")
        
# Show all plots
if show_plots:
    plt.show()