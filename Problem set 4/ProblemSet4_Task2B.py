""" Python script for "Numerical Methods in Differential Equations" classes.
    Problem set: 4
    Task: 2B.
"""
from matplotlib import rcParams
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import itertools
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Prepare directory for saving figures
def path(file_name, dbgprint=True):
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    figure_dir = os.path.join(script_dir, "fig_T2B")
    
    # Create figure_dir if don't exist
    if not os.path.exists(figure_dir):
        os.mkdir(figure_dir)
    
    file_path = os.path.join(figure_dir, file_name)
    if dbgprint:
        print(F"Plot saved to \"{file_path}\".\n")
    return file_path



# -----------| Task 4D solution |---------------------
rcParams['figure.figsize'] = 10, 8
rcParams["figure.autolayout"] = True # plt.tight_layout()
show_plots = True
print('-'*100)
print('Python script for "Numerical Methods in Differential Equations" classes\nProblem set: 5\nTask: 2B\n')

# Exact solution u_d
def exact_solution_formula(x, t):
    return (t * np.sin(2 * x + t))

# a(x,t) coefficient function
def a_coefficient(x=None, t=None):
    return 0.5  # for this task a is constant and positive

# f(x, t) function
def func_x_t(x,t):
    return np.sin(2*x+t)

# Boundary vector - vector_v(x) - horizontal vector for t = 0 (x is changing)
def vector_v(x=None):
    return 0*x

# Boundary vector - vector_psi(x) - vertical vector for x = pi (t is changing)
def vector_psi(t):
    return t*np.sin(t)

# -----------| Parameters |--------------------
# Grid parameters
grid_a = 0      # first horizontal value
grid_b = np.pi  # last horizontal value
grid_c = 0      # first vertical value
grid_d = np.pi  # last vertical value
grid_n_set = np.array([50, 100, 200, 400, 800]) # Number of horizontal points
grid_lambda_set = np.array([0.1, 0.2, 0.3, 0.4])

# Arrays for comparing results
cmp_global_errors = np.zeros((grid_n_set.size, grid_lambda_set.size))
cmp_x_step = np.zeros((grid_n_set.size, grid_lambda_set.size))

for (n_id, grid_n), (lambda_id, grid_lambda) in itertools.product(enumerate(grid_n_set), enumerate(grid_lambda_set)):
    grid_m = int(grid_n * grid_lambda)
    grid_x_vector = np.linspace(grid_a, grid_b, grid_n)
    grid_t_vector = np.linspace(grid_c, grid_d, grid_m)
    grid_x_step = (grid_b - grid_a)/grid_n  # h
    grid_t_step = (grid_d - grid_c)/grid_m  # k
    cmp_x_step = grid_x_step
    print(F"\nGrid parameters: n: {grid_n}, m: {grid_m}, h: {grid_x_step:.5f}, k: {grid_t_step:.5f}, lambda: {grid_lambda}")
    grid_x, grid_t = np.meshgrid(grid_x_vector, grid_t_vector)
    
    # ---------| Plot grid only |------------------------
    fig, ax = plt.subplots()
    
    for x,y in zip(grid_x, grid_t):
        ax.plot(x, y, 'o', color="red", markersize=0.5)
    
    ax.set_title(F"Grid {grid_n}x{grid_m}\nh: {grid_x_step:.6f}\nk: {grid_t_step:.6f}\n lambda: {grid_lambda}")
    ax.set_xlabel("x")
    ax.set_ylabel("t")
    ax.grid(True)
    plt.tight_layout()
    fig.savefig(path(F"PS{'4'}_T{'2B'}_grid{grid_n}x{grid_m}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
        
    #---------| Prepare & plot exact solution |--------------------------------
    exact_solution = exact_solution_formula(grid_x, grid_t)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(grid_x, grid_t, exact_solution)
    ax.set_title(F"Exact solution {grid_n}x{grid_m}\nu_d(x,t) = t*sin(2x+t)\nlambda: {grid_lambda}")
    ax.set_xlabel("x")
    ax.set_ylabel("t")
    ax.set_zlabel("u_d(x, t)")
    plt.tight_layout()
    fig.savefig(path(F"PS{'4'}_T{'2B'}_exact_solution_{grid_n}x{grid_m}_lambda_{grid_lambda}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
        
    # ---------| Applying backward implicit scheme |--------------
    # prepare A matrix - a_ij = const so we can prepare A one and use it in loop
    diag0 = np.full(grid_n, (1+ grid_lambda * a_coefficient()))
    diag1 = np.full(grid_n-1, (-grid_lambda * a_coefficient()))
    A_matrix = np.diag(diag0, 0) + np.diag(diag1, 1)
    print("A_matrix = \n", A_matrix)
    print("det(A_matrix) =", np.linalg.det(A_matrix))
    
    # prepare matrix for approximation results
    approx_sol = np.zeros((grid_m, grid_n))
    
    # Assign boundary values
    approx_sol[0, :] = vector_v(grid_x_vector)
    approx_sol[:, -1] = vector_psi(grid_t_vector)
    
    # Iterate through rows
    for t_id in range(1, grid_m):
        kf = grid_t_step * func_x_t(grid_x[t_id, :], grid_t[t_id, :])
        w = grid_lambda * a_coefficient() * (np.append(np.zeros(grid_n-1), approx_sol[-1, t_id]))
        u = approx_sol[t_id-1, :]
        approx_sol[t_id, :] = np.linalg.solve(A_matrix, (u + w + kf))
    
    # Display aproximated solution
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(grid_x, grid_t, approx_sol)
    ax.set_title(F"Approximated solution {grid_n}x{grid_m}\nlambda: {grid_lambda}")
    ax.set_xlabel("x")
    ax.set_ylabel("t")
    ax.set_zlabel("approx solution(x, t)")
    plt.tight_layout()
    fig.savefig(path(F"PS{'4'}_T{'2B'}_approx_solution_{grid_n}x{grid_m}_lambda_{grid_lambda}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
            
    # Calculate pointwise error
    approx_errors = np.abs(approx_sol - exact_solution)
    
    # Print approximation error plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(grid_x, grid_t, approx_errors)
    ax.set_title(F"Approximation errors {grid_n}x{grid_m}\nlambda: {grid_lambda}")
    ax.set_xlabel("x")
    ax.set_ylabel("t")
    ax.set_zlabel("approx errors")
    plt.tight_layout()
    fig.savefig(path(F"PS{'4'}_T{'2B'}_approx_errors_{grid_n}x{grid_m}_lambda_{grid_lambda}.png"), bbox_inches='tight')
    if not show_plots:
        plt.close(fig)
    
    # Find the global error of approximation e(h,λ) = max |up(xi,tj ) −ud(xi,tj )|
    cmp_global_errors[n_id, lambda_id] = approx_errors.max()
    print(F"For h = {grid_x_step:.5f}, λ = {grid_lambda}, global error = {cmp_global_errors[n_id, lambda_id]:.3f}")
    
    # Create subplots: y_approx, y_exact and approx error for t=pi
    fig, (ax_result, ax_errors) = plt.subplots(nrows=2, sharex='row')
   
    # Plotting of y_approx and y_exact
    ax_result.set_title(F"Results of approximation for {grid_n}x{grid_m}\nlambda: {grid_lambda}\ngraphs for t = pi.")
    ax_result.set_xlabel("t")
    ax_result.set_ylabel("solution")
    ax_result.grid(True)
    ax_result.plot(grid_x[-1, :], approx_sol[-1, :], '.', color="green", label="approximated")
    ax_result.plot(grid_x[-1, :], exact_solution[-1, :], '-', color="red", label="exact")
    ax_result.legend()
    
    # Ploting of approx error
    ax_errors.set_title(F"Error of approximation for {grid_n}x{grid_m}\nlambda: {grid_lambda}\ngraphs for t = pi.")
    ax_errors.set_xlabel("t")
    ax_errors.set_ylabel("Approximation error")
    ax_errors.grid(True)
    ax_errors.plot(grid_x[-1, :], abs(approx_sol[-1, :] - exact_solution[-1, :]), '.', color="purple", label="approximation error")
    ax_errors.legend()
    
    plt.tight_layout()
    fig.savefig(path(F"PS{'4'}_T{'2B'}_approx_results_t3_14_{grid_n}x{grid_m}_lambda_{grid_lambda}.png"), bbox_inches='tight')    
    if not show_plots:
        plt.close(fig)
    
for (n_id, grid_n), (lambda_id, grid_lambda) in itertools.product(enumerate(grid_n_set), enumerate(grid_lambda_set)):
    grid_x_step = (grid_b - grid_a)/grid_n  # h
    print(F"For h = {grid_x_step:.5f}, λ = {grid_lambda}, global error = {cmp_global_errors[n_id, lambda_id]:.3f}")

if show_plots:
    plt.show(block=False)
    input("Press any key to close all plots >>>>")
    plt.close("all")