import matplotlib.pyplot as plt
import numpy as np

from mpl_toolkits.mplot3d import Axes3D

def monopole(x, y):
    r = np.sqrt(x**2 + y**2)
    return np.exp(-1j * 20 * r) / r

x = np.linspace(-1, 1, 50)
y = np.linspace(-1, 0, 100)

X, Y = np.meshgrid(x, y)
Z = np.real(monopole(X, Y))

fig, ax = plt.subplots(subplot_kw={'projection': '3d'})
ax.plot_surface(X, Y, Z)
plt.show()